#! /bin/sh
#  verify with cosign and private fulcio/rekor instance
# 1st argument = e-mail of signer
# 2nd argument = image

. ./env.sh

if [ $# -ne 2 ]; then
  echo "Usage: $0 email-of-signer image-or-object-to-verify"
  exit 0
fi

SIGNER=$1
IMAGE=$2

cosign verify  $IMAGE \
  --certificate-identity $SIGNER \
  --certificate-oidc-issuer  $COSIGN_OIDC_ISSUER_URL\
  --rekor-url $COSIGN_REKOR_URL \
  $IMAGE | jq .
