# What is needed to verify a keyless signature in cosign

If you want to verify a keyless signature with cosign with a private installation of fulcio & rekor you need the following info:

For every self hosted fulcio-rekor instance you have to get (or supply) the following infos. These can be public shared., because there's no sensitive information in it:

- rekor log public key
  - stored in the ENV variable `SIGSTORE_REKOR_PUBLIC_KEY` (link to file)
  - can be retrieved by with curl: https://REKOR_URL/api/v1/publickey
- tuf root certificate
  - stored in the ENV variable `SIGSTORE_ROOT_FILE` (link to file)
  - lies in the namespace as *secret/fulcio-server-secret* (.data.cert)
- ctfe public key
  - stored in the ENV variable `SIGSTORE_CT_LOG_PUBLIC_KEY_FILE` (link to file)
  - lies in the namespace as *secret/...ctlog-secret* (.data.public)
- rekor url
  - the https URL of your rekor instance. If you're hosting the system in an intranet, this is the only URL you have to share with the possible validators of your assets.
  - this is the URL of the openshift route (or k8s ingress) *rekor-route*

For the verification of individual assets you maybe have different signers. These are specified with 2 parameters:

- certificate oidc issuer
  - the OIDC issuer (URI) where the signer is logging in to verify his identity (typically an email address)
  - `--certificate-oidc-issuer=''` 
    - example: `--certificate-oidc-issuer=https://ig-bvc-dex-route-sigstore.apps.mirkwood.pfpk.pro/auth` for the example (using dex) with keycloak behind or
  - `--certificate-oidc-issuer-regexp=''` => specify a regexp for the OIDC issuer
    - example: `--certificate-oidc-issuer-regexp='.*'` to trust every OIDC issuer in this rekor instance
- certificat identity
  - the email address of the OIDC identity which signed the asset
  - `--cerificate-identity`
    - example: `--certificate-identity=peter@pflaeging.net` trust a fixed identity or
  - `certificate-identity-regexp=''`
    - example: `certificate-identity-regexp='.+@pflaeging.net'` to trust every signer with an email address (verified by OIDC) inside the organisation.

You always have to supply a pair od OIDC issuer and certificate identity to the verify process.

## Example

Based on the description above:

- if you run the script `get-keys+set-variables.sh` after logging in your cluster, the system gives you all the data from the fulcio-rekor instance:
  - a directory ('./certs') with the 3 files:
    -  SIGSTORE_REKOR_PUBLIC_KEY -> ./certs/publicKey.pem
    - SIGSTORE_ROOT_FILE -> ./certs/fulcio-root.pem
    - SIGSTORE_CT_LOG_PUBLIC_KEY_FILE -> ./certs/ctfe_public.pem
- the rekor URL (in my example: <https://ig-bvc-rekor-route-sigstore.apps.mirkwood.pfpk.pro>)
- the OIDC issuer (in my example: `https://ig-bvc-dex-route-sigstore.apps.mirkwood.pfpk.pro/auth` (no need to export it to the public!))
- the issuer identity (in my example I'm trusting all my people `.+@pflaeging.net`)

OK, here's the complete example:

```shell
export SIGSTORE_REKOR_PUBLIC_KEY=$PWD/certs/publicKey.pem
export SIGSTORE_ROOT_FILE=$PWD/certs/fulcio-root.pem
export SIGSTORE_CT_LOG_PUBLIC_KEY_FILE=$PWD/certs/ctfe_public.pem
cosign verify \
  --rekor-url='https://ig-bvc-rekor-route-sigstore.apps.mirkwood.pfpk.pro' \
  --certificate-oidc-issuer='https://ig-bvc-dex-route-sigstore.apps.mirkwood.pfpk.pro/auth' \
  --certificate-identity-regexp='.+@pflaeging.net' \
  reg.pflaeging.net/sig-poc/alpine-local-sign:3 \
  | jq
```
