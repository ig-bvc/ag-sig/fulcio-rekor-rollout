#! /bin/sh
# Sign with cosign and private fulcio/rekor instance

. ./env.sh

if [ $# -ne 1 ]; then
  echo "Usage: $0 image-or-object-to-sign"
  exit 0
fi

IMAGE=$1

cosign sign --oidc-issuer $COSIGN_OIDC_ISSUER_URL $IMAGE
