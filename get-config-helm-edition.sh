#! /bin/sh
## get and set variables for rekor/fulcio here

if [ $# -ne 2 ]; then
  echo "Usage: $0 namespace instancename "
  echo "     You can find them with"
  echo "     kubectl get deployments "
  echo "     and look for the beginning of the deployment names"
  exit 0
fi

# config variables here
namespace=$1
name=$2

here=$(dirname $0)

## TUF and certs
export REKOR_REKOR_SERVER=$(kubectl get ingress -l app.kubernetes.io/name=rekor,app.kubernetes.io/instance=$name -n $namespace --no-headers | awk '{print $3}')
export COSIGN_REKOR_URL=https://$REKOR_REKOR_SERVER

certstore=./${namespace}.${name}.configs
mkdir -p $certstore

kubectl get secret ctlog-secret -o jsonpath='{.data.public}' -n $namespace| base64 -d > $certstore/ctfe_public.pem
kubectl get secret fulcio-server-secret -o jsonpath='{.data.cert}' -n $namespace| base64 -d > $certstore/fulcio-root.pem
curl --silent -L -k ${COSIGN_REKOR_URL}/api/v1/log/publicKey -o $certstore/publicKey.pem
cp $here/sign-image.sh $here/verify-image.sh $certstore/


export SIGSTORE_CT_LOG_PUBLIC_KEY_FILE="./ctfe_public.pem"


## OIDC
export OIDC_ISSUER_URL=$(kubectl get cm -l app.kubernetes.io/name=fulcio,app.kubernetes.io/instance=$name -o jsonpath='{.items[0].data.config\.json}' -n $namespace | grep IssuerURL | cut -d '"' -f 4)
export COSIGN_OIDC_ISSUER_URL=$OIDC_ISSUER_URL
export SIGSTORE_OIDC_ISSUER=$OIDC_ISSUER_URL
## Rekor
export REKOR_REKOR_SERVER=$REKOR_REKOR_SERVER
export SIGSTORE_REKOR_PUBLIC_KEY="./publicKey.pem"
## Fulcio
export COSIGN_FULCIO_URL=https://$(kubectl get ingress -l app.kubernetes.io/name=fulcio,app.kubernetes.io/instance=$name -n $namespace --no-headers | awk '{print $3}')
export SIGSTORE_FULCIO_URL=$COSIGN_FULCIO_URL
export SIGSTORE_ROOT_FILE="./fulcio-root.pem"
## cosign specific
export COSIGN_OIDC_CLIENT_ID=$(kubectl get cm -l app.kubernetes.io/name=fulcio,app.kubernetes.io/instance=$name -o jsonpath="{.items[0].data.config\.json}" -n $namespace | grep ClientID | cut -d '"' -f 4)
export COSIGN_YES="true"



cat << EOF > $certstore/env.sh
#! /bin/sh
export COSIGN_FULCIO_URL=$COSIGN_FULCIO_URL
export COSIGN_OIDC_CLIENT_ID=$COSIGN_OIDC_CLIENT_ID
export COSIGN_OIDC_ISSUER_URL=$COSIGN_OIDC_ISSUER_URL
export COSIGN_REKOR_URL=$COSIGN_REKOR_URL
export COSIGN_YES="true"
export OIDC_ISSUER_URL=\$COSIGN_OIDC_ISSUER_URL
export REKOR_REKOR_SERVER=$REKOR_REKOR_SERVER
export SIGSTORE_CT_LOG_PUBLIC_KEY_FILE=$SIGSTORE_CT_LOG_PUBLIC_KEY_FILE
export SIGSTORE_FULCIO_URL=\$COSIGN_FULCIO_URL
export SIGSTORE_OIDC_ISSUER=\$COSIGN_OIDC_CLIENT_ID
export SIGSTORE_REKOR_PUBLIC_KEY=$SIGSTORE_REKOR_PUBLIC_KEY
export SIGSTORE_ROOT_FILE=$SIGSTORE_ROOT_FILE
EOF
