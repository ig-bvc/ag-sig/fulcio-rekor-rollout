# ToDo's from 2024-03-12

- [x] Add image mirror script for disconnected operation
- [x] Add alternative images in kustomize
- [x] Documentation: kustomization.yaml and fulcio-rekor-rollout.yaml
- [ ] Testing
- [ ] Test rollout in disconnected env
- [x] Tests in plain kubernetes (rollout works)
- [x] What do we do if no cert-manager is available or working
