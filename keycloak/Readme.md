# KeyCloak install and settings

- You have to patch the correct hostname in `ingress.yaml` or let your kustomize do it
- Rollout is done via: `oc | kubectl apply -k .`
- This installs a single potgresql server and a single keycloak instance
- The keycloak instance is replicabable (inifinspan config included)

## Add a realm and the sigstore service automagical

```shell
# get keycloak username and password
KCUSER=$(kubectl get secret keycloak-admin -o jsonpath='{.data.username}' | base64 -d)
KCPASS=$(kubectl get secret keycloak-admin -o jsonpath='{.data.password}' | base64 -d)
# login to keycloak
kubectl exec -ti $(kubectl get pod -l app=keycloak -o name) -- /opt/keycloak/bin/kcadm.sh config  credentials --server http://localhost:8080 --realm master --user $KCUSER --password $KCPASS --config /tmp/keycloak.json
# create realm
kubectl exec -ti $(kubectl get pod -l app=keycloak -o name) -- /opt/keycloak/bin/kcadm.sh create  realms -s realm=sigstore-dev -s displayName="Sigstore Development Realm" -s enabled=true --config /tmp/keycloak.json
# create client for sigstore
kubectl exec -i $(kubectl get pod -l app=keycloak -o name) -- /opt/keycloak/bin/kcadm.sh create  clients -r sigstore-dev -s clientId=sigstore -s redirectUris='["*"]' -s enabled=true -s publicClient=true --config /tmp/keycloak.json
```

(This is also here as `create-initial-keycloak-config.sh`)

Though you have to:

```shell
kubectl config set-context --namespace my-sigstore-namespace --current
sh ./create-initial-keycloak-config.sh
```
