#! /bin/sh
# get keycloak username and password

CLIENTNAME=$(kubectl get cm testrollout-fulcio-server-config -o yaml | grep '"ClientID":' | cut -d\" -f4)


KCUSER=$(kubectl get secret keycloak-admin -o jsonpath='{.data.username}' | base64 -d)
KCPASS=$(kubectl get secret keycloak-admin -o jsonpath='{.data.password}' | base64 -d)
# login to keycloak
kubectl exec -ti $(kubectl get pod -l app=keycloak -o name) -- /opt/keycloak/bin/kcadm.sh config  credentials --server http://localhost:8080 --realm master --user $KCUSER --password $KCPASS --config /tmp/keycloak.json
# create realm
kubectl exec -ti $(kubectl get pod -l app=keycloak -o name) -- /opt/keycloak/bin/kcadm.sh create  realms -s realm=sigstore-dev -s displayName="Sigstore Development Realm" -s enabled=true --config /tmp/keycloak.json
# create client for sigstore
kubectl exec -i $(kubectl get pod -l app=keycloak -o name) -- /opt/keycloak/bin/kcadm.sh create  clients -r sigstore-dev -s clientId=$CLIENTNAME -s redirectUris='["*"]' -s enabled=true -s publicClient=true --config /tmp/keycloak.json