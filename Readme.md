# Install fulcio and rekor for cosign use

Our main goal is to get it running in OpenShift / OKD 4.x, because the security considerations are the strongest in this env!

This work is mainly inspired by the famous [Sigstore the Hard Way](https://stacklok.github.io/sigstore-the-hard-way/) and should be named  
***Sigstore the Kubernetes / OpenShift way***  
:-)
## Prerequisites

- Create namespace "sigstore"
- local installation of
  - kubectl
  - cosign
  - jq
  - docker, podman, rancher-desktop, ...

## Rollout with an upper git repo (GitOps style)

*Please read the complete doc and come back here ;-)*

The best and easiest way to make a rollout is:

- Create your own repo
- Use this repo as submodule
- Overwrite only the parameters specific to your deployment

OK, let's do it:

```shell
# create repo
mkdir my-personal-fulcio-dev
cd my-personal-fulcio-dev
git init
git add .
git commit -a -m "Initial"
# add submodule
git submodule add git@gitlab.opencode.de:ig-bvc/ag-sig/fulcio-rekor-rollout.git fulcio-rekor-rollout
git submodule init
git submodule update --remote --init
# copy content from submodule
cp fulcio-rekor-rollout/template-for-upper-repo/* .
git add . 
git commit -a -m "Get template content"
```

Now you have to edit `fulcio-rekor-rollout-values.yaml` and `kustomization.yaml` to satisfy your prereqs

After this rollout like it is mentioned in [./template-for-upper-repo/Readme.md](./template-for-upper-repo/Readme.md)

## Config

Configuration is done in two places:

- [./kustomization.yaml](./kustomization.yaml)
- [./fulcio-rekor-rollout-values.yaml](./fulcio-rekor-rollout-values.yaml)

## fulcio & rekor install

### *optional*: Mirror Container Images for a disconnected rollout:

- Generate an imagelist with (be sure to set the last replacement to your correct registry):

  ```shell
  # make a csv for image mirroring
  kubectl kustomize . --enable-helm | grep image: | awk '{print $2}' | sort -u \
     | sed  -E "s/(.+\/)(.+)(:.*)/\1\2\3,MYNEWREGISTRY\/\2:production/g" \
     | sed -E "s/@sha256(:production)$/\1/g" \
     | sed -E "s/MYNEWREGISTRY/default-route-openshift-image-registry.apps.a2.cp.cna.at\/brz-sigstore-dev/g" \
     > imagelist.csv
  ```

- login to your destination registry with `skopeo login ...`
- Mirror with our minimal mirror script `./image-mirror.py imagelist.csv` (you can use -n for simulation or -v for verbose)
- Add the mirrors to a new kustomization.yaml with the following:

  ```shell
  # create a image mirror addendum for kustomize
  sed  -E "s/(.+\/)(.+)(:.*),(.+):production/- name: \1\2\n  newName: \4\n  newTag: production/g" \
  < imagelist.csv \
  | sed "s/@sha256//g" \
  >> imagemirror.add.yaml
  ```

- add the content of the file `imagemirror.add.yaml` at the end of `kustomization.yaml`
- You're done, the rest of th rollout should work

### Rollout

- method:
  - install the helm charts from <https://github.com/sigstore/helm-charts>
  - patch them with kustomize in place for OpenShift / OKD 4 compatiblity
- `kubectl create namespace my-sigstore-dev` or  for openshift/okd `oc new-project my-sigstore-dev`
- `helm repo add sigstore https://sigstore.github.io/helm-charts`
- `helm dependency build fulcio-rekor-rollout/`
- `kubectl kustomize --enable-helm . | kubectl apply -f -`
- this will install fulcio, rekor and trillian in your project/namespace and generates the three required routes
- the trillian createdb job also crashes about 2 or 3 times, becuse the database is not there
- a keycloak installation is also rolled out.
- follow the tasks to setup the keycloak realm and client ( [./keycloak/Readme.md](./keycloak/Readme.md) )

    ```shell
    cd keycloak
    kubectl config set-context --namespace my-sigstore-namespace --current
    sh ./create-initial-keycloak-config.sh
    ```

- create yourself a user or connect your ActiveDirectory in KeyCloak. You can access KeyCloak via your configured Ingress. The Admin user and password is in the secret `keycloak-admin`


## Usage

```console
# login to your registry
docker login reg.pflaeging.net
# push image
docker push reg.pflaeging.net/sig-poc/alpine-local-sign:3
# generate config
./get-config-helm-edition.sh brz-sigstore-dev testrollout
# jump in the generated config directory
cd brz-sigstore-dev.testrollout.configs
# sign image
../sign-image.sh reg.pflaeging.net/sig-poc/alpine-local-sign:cosign-helm-signed
# verify signature
../verify-image.sh peter@pflaeging.net reg.pflaeging.net/sig-poc/alpine-local-sign:cosign-helm-signed
```

## External verification

If you want to enable someone external to check your images you have to do the following:

- Expose your rekor URL (so that the external verifier can access the URL).  
  Here it's something like `https://ig-bvc-fulcio-route-sigstore.apps.my.cluster.name`
- Execute the small script `./get-config-helm-edition.sh` with the namespace and the instance name (for example):
  `./get-config-helm-edition.sh brz-sigstore-dev testrollout`
- This generates a firectory with the 3 required public certs and a small shell-script with the environment
- Your partner needs the following env:
  - cosign binary
  - jq binary (to make the output beauty)
  - this directory
  - access to your fulcio via https
  - access to a container registry where the signed image is ;-)

(BTW: the Usage an External verification is tested only on unix-like OS like Linux or MacOS. I don't know if it' working on Windows  
-peter)

The verification process and the needed parts are better specified in [./WhatIsNeededForVerify.md](./WhatIsNeededForVerify.md).

## to mention

- You have to create your user for this test instance yourself
- Access to your Identity is here: <https://my-keacloak-hostname-defined-in-kustomize.dev/realms/sigstore-dev/account/>

## ToDo

look in [./ToDo.md](./ToDo.md)
