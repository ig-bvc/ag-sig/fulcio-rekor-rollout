# Rollout in cluster GitOps style

## Edit configs

- kustomization.yaml
- fulcio-rekor-rollout.yaml
- route-*.yaml if needed

## Rollout

- `helm repo add sigstore https://sigstore.github.io/helm-charts`
- `helm dependency build fulcio-rekor-rollout/fulcio-rekor-rollout/`
- Wait for completion of the rollout
- If you have rolled out keycloak please install the realm and the client with:  
  `./fulcio-rekor-rollout/keycloak/create-initial-keycloak-config.sh`
- otherwise create a client in your Keycloak / Dex / Other Idp: like in `./fulcio-rekor-rollout/keycloak/kc-client-definition-sigstore-dev.json`
- Add Users to your Keycloak instance or make an LDAP bind with your Active Directory

## Get config

Use the script `./fulcio-rekor-rollout/get-config-helm-edition.sh` to get the config and some example scripts for signing and verify.

## Minimal first step docker container

There's a Dockerfile here for a minimal automation support

Build the container with:  
`podman build -t reg.pflaeging.net/brz/cosign-executor:latest .`  
(docker build works also)

Run it with:
`podman run --name cosign --rm -ti --volume $PWD:/data reg.pflaeging.net/brz/cosign-executor:latest bash`

If you're inside the container try:

```shell
# go to the generated dir in the container
cd /data/mynamespace.testrollouts.configs
# login to your registry
podman login -u myusername registry.my.org
# sign image
./sign-image.sh registry.my.org/myproject/mycontainer:v1.0
# You get an URL: Paste it in your browser and login. 
# - You will get a verification code back
# - Paste this verification code here and press enter
# You should get a signed image
#
```
